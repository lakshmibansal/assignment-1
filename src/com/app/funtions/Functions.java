/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.funtions;

import com.app.entities.CustomerDetails;

import java.util.*;

/**
 *
 * @author LAKSHMI BANSAL
 */
public class Functions {

    Map<Integer, CustomerDetails> customerDB = new HashMap<Integer, CustomerDetails>();
    public static Set<Integer> carIdList = new HashSet<>();

    public void addCustomer() {
        int id;
        String name;
        System.out.println("Enter customer ID : ");


        if (CustomerDetails.cInput.hasNextInt()) {
            id = CustomerDetails.cInput.nextInt();
            CustomerDetails.cInput.nextLine();
            if (!customerDB.containsKey(id)) {
                System.out.println("Enter customer Name");
                if (CustomerDetails.cInput.hasNextLine()) {
                    name = CustomerDetails.cInput.nextLine();
                    CustomerDetails customer = new CustomerDetails(id, name);
                    customerDB.put(id, customer);
                } else {
                    System.out.println("Enter Valid Name");
                }
            } else {
                System.out.println("Customer ID already exists.");
            }
        } else {
            System.out.println("Enter valid ID !!");
            CustomerDetails.cInput.nextLine();
        }

    }

    public void addCar() {
        int id;
        if (customerDB.size() > 0) {
            System.out.println("Enter customer ID : ");


            if (CustomerDetails.cInput.hasNextInt()) {
                id = CustomerDetails.cInput.nextInt();
                CustomerDetails.cInput.nextLine();
                if (customerDB.get(id) != null) {
                    CustomerDetails CustomerObj = customerDB.get(id); // O(1)
                    CustomerObj.addCarToCustomer();
                } else {
                    System.out.println("Customer ID does not exists");
                }

            } else {
                System.out.println("Enter valid ID !!");
                CustomerDetails.cInput.nextLine();
            }
        } else {
            System.out.println("No Customer record exists");
        }

    }

    public void displayCustomer() {
        int id;


        if (customerDB.size() < 1) {
            System.out.println("No Record Found");
        } else {
            System.out.println("Enter customer ID : ");
            if (CustomerDetails.cInput.hasNextInt()) {
                id = CustomerDetails.cInput.nextInt();
                CustomerDetails.cInput.nextLine();
                if (customerDB.get(id) != null) {
                    CustomerDetails CustomerObj = customerDB.get(id);
                    CustomerObj.display();
                    System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
                } else {
                    System.out.println("Customer ID does not exists");
                }
            } else {
                System.out.println("Enter valid ID !!");
                CustomerDetails.cInput.nextLine();
            }
        }
    }

    public void displayAll() {
        if (customerDB.size() < 1) {
            System.out.println("No Record Found");
        } else {
            List<CustomerDetails> customerList = new ArrayList<>();
            for (Map.Entry<Integer, CustomerDetails> entry : customerDB.entrySet()) {
                customerList.add(entry.getValue());
            }

            // Sort list with comparator, to compare the Map values
            Collections.sort(customerList, new Comparator<CustomerDetails>() {
                public int compare(CustomerDetails o1,
                        CustomerDetails o2) {
                    return (o1.getName()).compareTo(o2.getName());
                }
            });

            for (CustomerDetails entry : customerList) {
                entry.display();
            }

            System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
        }

    }

    public void prizeSelection() {

        Random random = new Random();
        int luckyDraw[] = new int[3];
        List<Integer> keys = new ArrayList<Integer>(customerDB.keySet());

        Set<Integer> luckyWinners = new HashSet<Integer>();
        if (keys.size() > 6) {
            for (int i = 0; i < 6; i++) {
                int value = keys.get(random.nextInt(keys.size()));
                if (luckyWinners.contains(value)) {
                    i--;
                }
                luckyWinners.add(value);

            }

            for (int i = 0; i < 3; i++) {
                System.out.println("Enter customer " + i + " for Luky draw : ");
                do {
                    if (CustomerDetails.cInput.hasNextInt()) {

                        luckyDraw[i] = CustomerDetails.cInput.nextInt();
                        CustomerDetails.cInput.nextLine();
                        break;
                    } else {
                        System.out.println("Enter Correct Choice");
                        CustomerDetails.cInput.nextLine();
                    }
                } while (true);
            }
            System.out.println("\t\t\tWinners are : \n ");
            for (int i = 0; i < 3; i++) {
                if (luckyWinners.contains(luckyDraw[i])) {
                    System.out.println("\t\t\tCustomer ID : " + luckyDraw[i]);
                }
            }
        } else {
            System.out.println("There should be atleast 6 customers for luckyDraw");
        }


    }
}