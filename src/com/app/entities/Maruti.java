/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.entities;

import com.app.utility.AppConstants;

/**
 *
 * @author LAKSHMI BANSAL
 */
public class Maruti extends CarDetails{
     public Maruti(int id) {
        super(id);
        setBrand(AppConstants.MARUTI);
        setModal();
    }
    
    @Override
   public void setResaleValue(){
       this.resaleValue = AppConstants.TOYOTA_RESALE*this.price;
   }         
     
}
