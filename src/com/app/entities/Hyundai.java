/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.entities;

import com.app.utility.AppConstants;

/**
 *
 * @author LAKSHMI BANSAL
 */
public class Hyundai extends CarDetails{
     public Hyundai(int id) {
        super(id);
        setBrand(AppConstants.HUNDAI);
        setModal();
    }
    
    @Override
   public void setResaleValue(){
       this.resaleValue = AppConstants.TOYOTA_RESALE*this.price;
   }         
     
}
