/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.entities;

import java.util.Scanner;

/**
 *
 * @author LAKSHMI BANSAL
 */
public abstract class CarDetails {
    //attributes

    private int id;
    protected float resaleValue;
    protected float price;
    private String brand;
    private String modal;
    private Scanner carInput = new Scanner(System.in);

    public CarDetails(int id) {
        this.id = id;
        setPrice();
    }
    
    
    //Abstact function
    public abstract void setResaleValue();

    
    //getters and Setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
       
        this.id = id;
    }

    public float getResaleValue() {
        return resaleValue;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice() {
        System.out.println("Enter Price");
        do {
            if (carInput.hasNextFloat()) {
                this.price = carInput.nextFloat();
                carInput.nextLine();
                break;
            } else {
                System.out.println("Enter a Valid Price!!");
                carInput.nextLine();
            }
        } while (true);
    }

    public String getModal() {
        return modal;
    }

    public void setModal() {
        System.out.println("Enter Model");
        do {
            if (carInput.hasNextLine()) {
                this.modal = carInput.nextLine();
                break;
            } else {
                System.out.println("Enter a Valid Model!!");
                carInput.nextLine();
            }
        } while (true);
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {

        this.brand = brand;
    }
    
    @Override
    public String toString() {
        return "\t\tCar ID : " + id+"\n\t\tCar Brand : "+brand+"\n\t\tCar Model : "+modal
                +"\n\t\tCar Price : "+price+"\n\t\tCar Resale Value : "+resaleValue; 
    }
   
}
