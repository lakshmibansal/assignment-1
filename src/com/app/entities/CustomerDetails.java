/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.entities;

import java.util.*;
import com.app.funtions.Functions;
import com.app.utility.AppConstants;

/**
 *
 * @author LAKSHMI BANSAL
 */
public class CustomerDetails {
    //attributes

    private int id;
    private String name;
    List<CarDetails> carList = new ArrayList<CarDetails>();
    public static Scanner cInput = new Scanner(System.in);

    public CustomerDetails(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId() {
        // do {
        if (cInput.hasNextInt()) {
            this.id = cInput.nextInt();
            cInput.nextLine();
            //break;
        } else {
            System.out.println("Enter a Valid ID!!");
            cInput.nextLine();
        }
        //} while (true);
    }

    public String getName() {
        return name;
    }

    public void setName() {
        do {
            if (cInput.hasNextLine()) {
                this.name = cInput.nextLine();
                break;
            } else {
                System.out.println("Enter a Valid name!!");
            }
        } while (true);

    }

    public void addCarToCustomer() {
        int id, option;
        boolean flag = false;
        System.out.println("Enter Car ID : ");

        if (cInput.hasNextInt()) {
            id = cInput.nextInt();
            cInput.nextLine();
            if (!Functions.carIdList.contains(id)) {
                System.out.println("\t\t1.Toyota");
                System.out.println("\t\t2.Hyundai");
                System.out.println("\t\t3.Maruti");


                System.out.println("Enter Your Choice : ");
                do {
                    if (cInput.hasNextInt()) {
                        option = cInput.nextInt();
                        cInput.nextLine();
                        Functions.carIdList.add(id);
                        CarDetails carobj;
                        switch (option) {
                            case AppConstants.TOYOTA_OPTION:
                                carobj = new Toyota(id);
                                carobj.setResaleValue();
                                carList.add(carobj);
                                break;
                            case AppConstants.HUNDAI_OPTION:
                                carobj = new Hyundai(id);
                                carobj.setResaleValue();
                                carList.add(carobj);
                                break;
                            case AppConstants.MARUTI_OPTION:
                                carobj = new Maruti(id);
                                carobj.setResaleValue();
                                carList.add(carobj);
                                break;
                            default:
                                System.out.println("Enter correct choice");
                                break;
                        }
                        flag = true;
                    } else {
                        System.out.println("Enter Correct choice");
                        cInput.nextLine();
                    }
                } while (flag == false);
            } else {
                System.out.println("car Id already exists");
            }
        } else {
            System.out.println("Enter valid ID !!");
            cInput.nextLine();
        }

    }

    @Override
    public String toString() {
        return "Customer Name : " + name+"\nCustomer ID : "+id; 
    }

    
    
    public void display() {
        
        
        System.out.println("---------------------------------");
        System.out.println(this);
        
        if (carList.size()>0) {
            System.out.println("Assosciated cars : \n");
            Collections.sort(carList, new Comparator<CarDetails>() {
                @Override
                public int compare(CarDetails t, CarDetails t1) {
                    return t.getModal().compareTo(t1.getModal());
                }
            });
            for (CarDetails c : carList) {
                System.out.println("************************");
                System.out.println(c);
                System.out.println("\n");
            }
        }
    }
}
