/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.entities;
import com.app.utility.AppConstants;

/**
 *
 * @author LAKSHMI BANSAL
 */
public class Toyota extends CarDetails {

    public Toyota(int id) {
        super(id);
        setBrand(AppConstants.TOYOTA);
        setModal();
    }
    
    @Override
   public void setResaleValue(){
       this.resaleValue = AppConstants.TOYOTA_RESALE*this.price;
   }         
            
}
