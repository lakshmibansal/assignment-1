/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.main;

import com.app.funtions.Functions;
import java.util.*;
import com.app.utility.AppConstants;

/**
 *
 * @author LAKSHMI BANSAL
 */
public class Main {

    public static void main(String[] args) {
        Scanner Choice = new Scanner(System.in);
        int option;
        Functions mainObj = new Functions();
        do {
            System.out.println("\n\t\t1.Add Customer");
            System.out.println("\t\t2.Add Car to Customer");
            System.out.println("\t\t3.Display All customer");
            System.out.println("\t\t4.Display specific Customer");
            System.out.println("\t\t5.Lucky Draw Prize Distribution");
            System.out.println("\t\t0.Exit\n\n");
            System.out.println("Enter Your Choice : ");
            if (Choice.hasNextInt()) {
                option = Choice.nextInt();
                Choice.nextLine();
            } else {
                System.out.println("Enter Correct choice");
                Choice.nextLine();
                continue;
            }
            switch (option) {
                case AppConstants.EXIT_CASE:
                    System.exit(0);

                case AppConstants.ADD_CUSTOMER:
                    mainObj.addCustomer();
                    break;

                case AppConstants.ADD_CAR:
                    mainObj.addCar();
                    break;

                case AppConstants.DISPLAY_ALL:
                    mainObj.displayAll();
                    break;

                case AppConstants.DISPLAY_CUSTOMER:
                    mainObj.displayCustomer();
                    break;
                case AppConstants.LUCKY_DRAW:
                    mainObj.prizeSelection();
                    break;
                default:
                    System.out.println("Enter Correct choice");
                    break;
            }
        } while (true);

    }
}
