/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.utility;

/**
 *
 * @author LAKSHMI BANSAL
 */
public interface AppConstants {

    String TOYOTA = "TOYOTA";
    String MARUTI = "MARUTI";
    String HUNDAI = "HUNDAI";
    
    
    // resale prices
    float TOYOTA_RESALE = (float) 0.8;
    float MARUTI_RESALE = (float) 0.6;
    float HUNDAI_RESALE = (float) 0.4;
    
    
    //switch case constants
    int EXIT_CASE = 0;
    int TOYOTA_OPTION = 1;
    int MARUTI_OPTION = 2;
    int HUNDAI_OPTION = 3;
    
    
    int ADD_CUSTOMER = 1;
    int ADD_CAR = 2;
    int DISPLAY_ALL = 3;
    int DISPLAY_CUSTOMER = 4;
    int LUCKY_DRAW = 5;
}
